import { Outlet } from "react-router-dom";
import { Navigation } from "../components/navigation";

export const Root = () => (
    <>
      <Navigation />
      <Outlet />
    </>
);
